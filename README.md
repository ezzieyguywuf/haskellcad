# HaskellCAD

Don't worry, I plan to fill this out. Check out
[mycad](https://gitlab.com/ezzieyguywuf/mycad), that's where this whole thing
started. I have to figure out if I'm going to migrate that project here, or
vice-versa, or merge them, or what.

Also, I'm probably going to rewrite history on this git repository, so beware.
Don't worry, I'll only do it once, then start doing the good stuff as outlined
in the mycad design goals and stuff - I was just anxious to get started on this
Haskell stuff, and it's sort of fun!
